import "./App.css";
import EX_LAYOUT_2 from "./Ex_Layout_2/EX_LAYOUT_2";
function App() {
  return (
    <div className="App">
      <EX_LAYOUT_2 />
    </div>
  );
}

export default App;
